**Bonus Task**

Create a pipeline that could choose a template to execute from the manual
job menu (Run pipeline).

Solution : The CICD Configuration file for this task can be found in the same project :

https://gitlab.com/lpassignment/bonusproject/-/blob/main/.gitlab-ci.yml?ref_type=heads

In order to run the pipeline goto Build-->Pipelines and click on Run Pipeline and you will see the expected Dashboard to select a template from the Run Pipeline Option.
